class itp():
    def __init__(self):
        self.sections=[]
        
    def read_itp(self, inFN):
        raw_itp=[]
        bsection=False
        bifdef=""
        with open(inFN) as f:
            for line in f:
                if (line.strip() == ""): continue
                if (line.strip()[0] == "#"): continue
                if (line.strip()[0] == "["):
                    bsection=True
                    self.sections.append([line.replace(" ", "")[1:-2],[],{}])
                    continue
                if (bsection):
                    if (line.strip()[0] == ";"): 
                        self.sections[-1][2].update({"comment":line.strip()}) 
                    else:
                        self.sections[-1][1].append(line.split())
                else:
                    pass
    
    def rename_molecule(self, name):
        self.sections[0][1][0][0] = name
        for atom in self.sections[1][1]:
             atom [3] = name
                
    def rm_atom(self, nat):
        for nsection, section in enumerate(self.sections):
            for nentry, entry in enumerate(section[1]):
                if nat in entry:
                    #print (section[0], entry)
                    self.sections[nsection][1][nentry] = []
        self.rm_empty_entries()
    
    # def add_atom(self, atom):
    #     for nentry, entry in enumerate(self.sections[1][1]):
    #         if atom["prev"] in entry:
    #             atoms_idx=nentry
    #     nat=int(self.sections[1][1][atoms_idx][0])
    #     self.shift_down_atoms(nat)
    #     #Real addition after moving all atoms down.
    #     atom_list=[str(nat+1),atom["atype"],atom["resnu"],atom["resname"],atom["aname"],str(nat+1),atom["q"],atom["m"]]
    #     self.sections[1][1].insert(nat, atom_list)
    #                 
        
    def shift_down_atoms(self, nat):
        for shift in range(len(self.sections[1][1]), nat, -1):
            for nsection, section in enumerate(self.sections):
                for nentry, entry in enumerate(section[1]):
                    if str(shift) in entry:
                        self.sections[nsection][1][nentry][entry.index(str(shift))] = str (shift+1)
                        #print (section[0], entry)
                    if str(shift) in entry:
                        self.sections[nsection][1][nentry][entry.index(str(shift))] = str (shift+1)
                        
    def change_atom_name(self, nat, new_aname):
        self.sections[1][1][nat-1][4]=new_aname
        
    def change_atom_type(self, nat, new_atype):
        self.sections[1][1][nat-1][1]=new_atype

    def change_atom_charge(self, nat, new_charge):
        self.sections[1][1][nat-1][6]=new_charge
    
    def change_atom_mass(self, nat, new_mass):
        self.sections[1][1][nat-1][7]=new_mass
    
    def rm_empty_entries(self):
        for nsection,section in enumerate(self.sections):
            self.sections[nsection][1] =  [x for x in section[1] if x != []]
    def copy_hydrogen(self,old_name, new_name):
        #Find hydrogen index
        for nentry, entry in enumerate(self.sections[1][1]):
            if old_name in entry:
                atoms_idx=nentry
        #Find atom number
        nat=int(self.sections[1][1][atoms_idx][0])
        #shift 1 number up all atoms number bigger than "nat"
        self.shift_down_atoms(nat)
        #Add atom "new_name" as a copy of "old_name"
        atom_list=self.sections[1][1][atoms_idx][:]
        atom_list[0] = str(int(atom_list[0])+1) #atom number
        atom_list[4] = new_name
        atom_list[5] = str(int(atom_list[5])+1) #charge group
        ##print(atom_list)
        self.sections[1][1].insert(nat, atom_list)
        #Duplicate every  bonded entry with "nat"
        new_iter=[]
        for nsection, section in enumerate(self.sections):
            if (section[0] == "moleculetype"): continue
            if (section[0] == "atoms"): continue
            for nentry,entry in enumerate(section[1]):
                if str(nat) in entry:
                    new_entry=entry[:]
                    for index, item in enumerate(new_entry):
                        if (item == str(nat)):
                            new_entry[index] = str(nat+1)
                            new_iter.append([nentry+1,new_entry])
            for niter,iter in enumerate(new_iter):
                self.sections[nsection][1].insert(iter[0],iter[1])
            new_iter=[]
    
    def write_itp(self, outFN):
        with open(outFN, "w") as f:
            for section in self.sections:
                print (section[0])
                f.write("[ {0} ]\n".format(section[0]))
                if section[2].get("comment"): 
                    f.write(section[2].get("comment"))
                    f.write("\n")
                if (section[0] == "moleculetype"):
                    format_out = "{0[0]:11s}{0[1]}\n"
                elif (section[0] == "atoms"):
                    format_out = "{0[0]:>6s}{0[1]:>11s}{0[2]:>7s}{0[3]:>7s}{0[4]:>5s}{0[5]:>9s}{0[6]:>11s}{0[7]:>11s}\n"
                elif (section[0] == "bonds" or section[0] == "pairs"):
                    format_out = "{0[0]:>5s}{0[1]:>6s}{0[2]:>6s} \n"
                elif (section[0] == "angles"):
                    format_out = "{0[0]:>5s}{0[1]:>6s}{0[2]:>6s}{0[3]:>6s} \n"
                elif (section[0] == "dihedrals"):
                    format_out = "{0[0]:>5s}{0[1]:>6s}{0[2]:>6s}{0[3]:>6s}{0[4]:>6s} \n"
                elif (section[0] == "position_restraints"):
                    format_out = "{0[0]:>6s}{0[1]:>6s}{0[2]:>6s}{0[3]:>6s}{0[4]:>6s}\n"
                else:
                    format_out = "{0}"
                for entry in section[1]:
                    #print(entry)
                    f.write(format_out.format(entry))
                f.write("\n") 
            

#Remove atom from topology in sequential order 

if __name__ == "__main__":
    
    # Read topology
    inFN="DOPS.itp"
    outFN="POPS.itp"
    

    POPS_itp=itp()
    POPS_itp.read_itp(inFN)
    POPS_itp.rename_molecule("POPS")

    #Double bond sn-1
    POPS_itp.change_atom_charge(100,"0.0")
    POPS_itp.change_atom_charge(101,"0.0")
    POPS_itp.change_atom_charge(102,"0.0")
    POPS_itp.change_atom_charge(103,"0.0")
    POPS_itp.change_atom_charge(104,"0.0")
    POPS_itp.change_atom_charge(105,"0.0")
    POPS_itp.change_atom_charge(106,"0.0")
    POPS_itp.change_atom_charge(107,"0.0")
    POPS_itp.change_atom_charge(108,"0.0")
    POPS_itp.change_atom_charge(109,"0.0")
    
    POPS_itp.change_atom_type(103, "CTL2")
    POPS_itp.change_atom_type(104, "HAL2")
    POPS_itp.change_atom_type(105, "CTL2")
    POPS_itp.change_atom_type(106, "HAL2")
    
    #Terminal
    POPS_itp.change_atom_charge(119,"0.047")
    POPS_itp.change_atom_charge(120,"-0.007")
    POPS_itp.change_atom_charge(121,"-0.007")
    POPS_itp.change_atom_charge(122,"-0.081")
    POPS_itp.change_atom_charge(123,"0.016")
    POPS_itp.change_atom_charge(124,"0.016")
    POPS_itp.change_atom_charge(125,"0.016")
    
    POPS_itp.change_atom_type(122, "CTL3")
    POPS_itp.change_atom_type(123, "HAL3")
    POPS_itp.change_atom_type(124, "HAL3")
    POPS_itp.change_atom_type(125, "HAL3")
    
    POPS_itp.change_atom_mass(125,"1.008")
    
    POPS_itp.change_atom_name(125,"H16Z")
    
    atoms_rm=range(131,125,-1)
    for natom in atoms_rm :
        POPS_itp.rm_atom(str(natom))
    
    POPS_itp.copy_hydrogen("H9X", "H9Y")
    POPS_itp.copy_hydrogen("H10X", "H10Y")
    
    # atom1={"prev":"H9X", "atype":"HAL2", "resnu":"1", "resname":"POPS", "aname":"H9Y", "q":"0.0", "m":"1.008"}
    # POPS_itp.add_atom(atom1)
    # atom2={"prev":"H10X", "atype":"HAL2", "resnu":"1", "resname":"POPS", "aname":"H10Y", "q":"0.0", "m":"1.008"}
    # POPS_itp.add_atom(atom2)
    #      
    POPS_itp.write_itp(outFN)